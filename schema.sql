drop table if exists dropbox_entries;
create table dropbox_entries (
	id integer primary key autoincrement,
	-- idDbx integer not null,
	dpath text not null,
	fpath text not null,
	tagsCSV text not null
);
