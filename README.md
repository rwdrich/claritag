## Synopsis

Claritag is a [Clarifai](http://clarifai.com) application that tags photos found in your [Dropbox](http://dropbox.com) account.

## Motivation

~~Yeah, because we want prizes~~ Because we wanted to create a cool app to automagically categorise our photos.

## Installation

Installing the dependencies can be achieved via `python-pip`, which will be installed as `pip` in a Python2 install.

```
# ensure you have python2 installed
which python2 || python --version

pip install -r requirements.txt
./setup_db.py
```

You will then need to set your app keys inside `config.py` for your Dropbox and Clarfai API access:

To set up Dropbox, you will need to [Create a Dropbox App](https://www.dropbox.com/developers/apps/create), with the following settings:

- a "Dropbox API" app
- "Full Dropbox" access
- Any name you want

NOTE: If you are running on Arch Linux, you will need to install `python2-dropbox` via `python2-dropbox-PKGBUILD` which has been grabbed from [the Arch Linux Bugtracker](https://bugs.archlinux.org/task/43929).

## Contributors

[rwdrich](https://gitlab.com/rwdrich), [jamietanna](https://gitlab.com/jamietanna), [frebib](https://gitlab.com/frebib)


## License

This software is licensed under the [MIT License](LICENSE).
