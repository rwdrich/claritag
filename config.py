#!/usr/bin/env python2
CLARIFAI_ID = "ENTER_CLARIFAI_ID_HERE"
CLARIFAI_SECRET = "ENTER_CLARIFAI_SECRET_HERE"
CLARIFAI_PREFIX = "https://api.clarifai.com/v1/tag/?url="

DROPBOX_ID = "ENTER_DROPBOX_ID_HERE"
DROPBOX_SECRET = "ENTER_DROPBOX_SECRET_HERE"
DROPBOX_REDIRECT_URI = "http://localhost:5000/dropbox-auth-finish"

DATABASE = "claritag.db"
DATABASE_SCHEMA = "schema.sql"

SECRET_KEY = "IReallyShouldSleepButYoloSwagettinaise"

TEMP_IMAGE_FOLDER = 'static/'
