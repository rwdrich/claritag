#!/usr/bin/env python2
import os
from flask import Flask, render_template, abort, g, session, redirect, request

from contextlib import closing

import clarifai

from dropbox.client import DropboxOAuth2Flow, DropboxClient
import dropbox

import uuid

import sqlite3
app = Flask(__name__)
app.config.from_pyfile('config.py')
app.config.update(dict(
	SECRET_KEY=app.config['SECRET_KEY']
))

def get_dropbox_media_url(fPath):
    # TODO ensure that session is set
    client = DropboxClient(session['access_token'])
    mediaDict = client.media(fPath)
    return mediaDict['url']

def init_db():
    with closing(connect_db()) as db:
        with app.open_resource(app.config['DATABASE_SCHEMA'], mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

@app.route('/')
def index():
	if session and session['access_token']:
		return redirect('/dropbox-clarifai-setup')
	return render_template('index.html', message="Welcome!", page_title="Welcome to Claritag")

@app.route('/dropbox-clarifai-setup')
def dropbox_clarifai_setup():
    client = DropboxClient(session['access_token'])
    for (dPath,mURL) in get_all_files():
        mTagsTpl = clarifai.getTags(clarifai.getAuth(), mURL)
        mTagsCSV = ",".join(mTagsTpl)

        fPath = str(uuid.uuid4()) + ".jpg"
        fOut = open(app.config['TEMP_IMAGE_FOLDER'] + fPath, 'wb')
        with client.get_file(dPath) as f:
            fOut.write(f.read())

        db = connect_db()
        db.execute("""insert into dropbox_entries (dpath, fpath, tagsCSV) values (?, ?, ?)""", (dPath, fPath, mTagsCSV)
                   )

        db.commit()

    return redirect('/tags')
    #return "Success..?"


@app.route('/tags')
def get_tags():
    db = connect_db()
    cur = db.cursor()
    tagList = []
    cur.execute("""select tagsCSV from dropbox_entries""")
    for row in cur.fetchall():
        for tag in row[0].split(','):
            tagList.append(tag)

    return render_template('tags.html', tags=tagList)


@app.route('/tag/<tagArg>')
def output(tagArg=None):
    db = connect_db()
    cur = db.cursor()
    query = '%' + tagArg + '%'
    cur.execute("select fpath from dropbox_entries where tagsCSV LIKE ?", (query,))
    r = cur.fetchall()
    return render_template('output.html', table=r)


def get_all_files():
    # TODO g.client
    client = DropboxClient(session['access_token'])

    dPaths = []

    metadata = client.metadata('/Photos')
    for dFile in metadata['contents']:
        print dFile
        # TODO all image types
        if not dFile['is_dir'] and (dFile['mime_type'] == 'image/jpg' or dFile['mime_type'] == 'image/jpeg' or dFile['mime_type'] == 'image/png'):
            dPaths.append(dFile['path'])

    mediaPaths = []
    for dFile in dPaths:
        mediaPaths.append(get_dropbox_media_url(dFile))
    return zip(dPaths, mediaPaths)

def get_dropbox_auth_flow(web_app_session):
    return DropboxOAuth2Flow(app.config['DROPBOX_ID'], app.config['DROPBOX_SECRET'],
                             app.config['DROPBOX_REDIRECT_URI'], web_app_session,
                             "dropbox-auth-csrf-token")

# URL handler for /dropbox-auth-start
@app.route('/dropbox-auth-start')
def dropbox_auth_start_route():
    return dropbox_auth_start(session, request)


def dropbox_auth_start(web_app_session, request):
    authorize_url = get_dropbox_auth_flow(web_app_session).start()
    return redirect(authorize_url)

# URL handler for /dropbox-auth-finish
@app.route('/dropbox-auth-finish')
def dropbox_auth_finish_route():
    return dropbox_auth_finish(session, request)

@app.route('/get/file/<fname>')
def get_file(fname=None):
    try:
        mURL = get_dropbox_media_url('Photos/' + fname)
    except dropbox.rest.ErrorResponse as e:
        return abort(e.status)

    return mURL

def dropbox_auth_finish(web_app_session, request):
    try:
        access_token, user_id, url_state = \
                get_dropbox_auth_flow(web_app_session).finish(request.args)
    except DropboxOAuth2Flow.BadRequestException as e:
        abort(400)
    except DropboxOAuth2Flow.BadStateException as e:
        # Start the auth flow again.
        return redirect("/dropbox-auth-start")
    except DropboxOAuth2Flow.CsrfException as e:
        abort(403)
    except DropboxOAuth2Flow.NotApprovedException as e:
        return redirect("/")
    except DropboxOAuth2Flow.ProviderException as e:
        logger.log("Auth error: %s" % (e,))
        abort(403)

    # if successful, then load the access key into our session
    # and then go home, ET
    session['access_token'] = access_token
    return redirect("/dropbox-clarifai-setup")

if __name__ == "__main__":
    if os.environ.get('DEBUG'):
        app.debug = True

    app.run()
