import requests
import sys
import json
import config

config.CLARIFAI_ID     = "SQDyUJ44D1stsnCUd7RKjo8BcfIM6XiTH0HMNDo9"
config.CLARIFAI_SECRET = "LSKBh_MLVMR5PNW6vdKUf167RpTa2JamEeYTZPac"
tag_count = 3

img_url = "http://www.flaniganphotography.com/data/photos/158_1baby_face.jpg"

def getAuth():
    payload = {'grant_type': 'client_credentials'}
    url = 'https://' + config.CLARIFAI_ID + ':' + config.CLARIFAI_SECRET + '@api.clarifai.com/v1/token/'
    a = requests.post(url, payload)
    return json.loads(a.content)["access_token"]

def getTags(auth, img_url):
    headers = { 'Authorization': 'Bearer ' + auth }
    url = config.CLARIFAI_PREFIX + img_url
    b = requests.get(url, headers=headers)
    res = json.loads(b.content)
    res = res['results'][0]['result']['tag']['classes']
    return res[:tag_count]

if __name__ == "__main__":
    if(len(sys.argv) > 0):
        img_url = sys.argv[1]
        tags = getTags(getAuth(), img_url)
        # print tags
